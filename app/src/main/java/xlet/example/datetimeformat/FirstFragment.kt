package xlet.example.datetimeformat

import android.os.Bundle
import android.text.format.DateFormat
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import xlet.example.datetimeformat.databinding.FragmentFirstBinding
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment() {

    private var _binding: FragmentFirstBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply {
            val defaultLocale = Locale.getDefault()
            val hour = if (DateFormat.is24HourFormat(context)) {
                "HH"
            } else {
                "hh"
            }
            val bestPatternYYYYMMdd = DateFormat.getBestDateTimePattern(defaultLocale, "yyyyMMdd")
            val bestPatternYYYYMMddHHmmss = DateFormat.getBestDateTimePattern(defaultLocale, "yyyyMMddE${hour}mmss")
            val date = Calendar.getInstance().time
            tvDtSdfYyyyMMdd.text = SimpleDateFormat("yyyy/MM/dd", defaultLocale).format(date)
            tvDtYyyyMMdd.text = SimpleDateFormat(bestPatternYYYYMMdd, defaultLocale).format(date)
            tvDtSdfYyyyMMddHHmmss.text = SimpleDateFormat("yyyy/MM/dd E ${hour}:mm:ss", defaultLocale).format(date)
            tvDtYyyyMMddHHmmss.text = SimpleDateFormat(bestPatternYYYYMMddHHmmss, defaultLocale).format(date)
            buttonFirst.setOnClickListener {
                findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}